#include <stdio.h>
#include <unistd.h>

int main()
{
	unsigned char c;
	while(scanf("%2x", &c) > 0) write(1, &c, 1);
	return 0;
}
