Simple and slow XOR encryption for small files

To install, compile btoh.c and htob.c:

gcc btoh.c -o btoh
gcc htob.c -o htob

The former converts stdin to a hex string, the later reverses the conversion.
The input/output can be binary or text.

Test:

echo "Stress test" | ./btoh | ./htob

The output should match the input.

Create the file phrase.txt:

$ >phrase.txt; chmod 600 phrase.txt

Edit phrase.txt with a text editor and insert a single line,
a character string long and varied enough that it can not be guessed.
Both the encryptor and decryptor must use the same secret phrase.

Encrypt a file:

$ cat file | ./sasxor.sh e > cipher.txt
$ rm file

Decrypt cipher.txt:

$ cat cipher.txt | ./sasxor.sh d > file

Test:

$ echo "Stress test" | ./sasxor.sh e | ./sasxor.sh d

The output should match the input.
