#!/bin/bash

# shared secret phrase known by encryptor and decryptor is in phrase.txt
#
# Encrypt: cat file | ./sasxor -e > cipher.txt
#
# Decrypt: cat cipher.txt | ./sasxor -d > file

function hexToHash { # convert hex string to 64-digit hex hash
 local hash=($(echo -ne $(echo "$1" | sed 's/../\\x&/g') | sha256sum))
 echo ${hash[0]}
}

function ones { # ones complement of 64-digit hex string
 local i; local c; local n=$1; local r=
 for ((i = 0; i < 64; i = i + 2)); do
  c=0x${n:i:2}
  r=$r$(printf %02x $((c ^ 0xff)))
 done
 echo $r
}

function xor {
  local i; local c
  for ((i = 0; i < length; i = i + 2)); do
    if ((i % 64 == 0)); then
      seed=$(hexToHash $seed)
      key=$(hexToHash $(ones $seed))
    fi
    c=0x${file:i:2}
    echo -n $(printf %02x $((c ^ 0x${key:i%64:2})))
  done
}

phrase="$(cat phrase.txt)"

if [[ $1 == "e" ]]; then
  file=$(cat | ./btoh); length=${#file}
  nonce=$(printf %016x $(($(date +%s%N)/1000000))) # 16-digit hex nonce
  echo -n $nonce
  seed="$phrase$nonce"
  xor
elif [[ $1 == "d" ]]; then
  file=$(cat)
  nonce=${file:0:16}
  file=${file:16}; length=${#file}
  seed="$phrase$nonce"
  xor | ./htob
else
  echo "Usage: 'sasxor e' for encryption, 'sasxor d' for decryption"
fi
